import Vuex from 'vuex'

const createStore = () => {
	return new Vuex.Store({
		state: () => ({
			counter: 0
		}),
		mutations: {
			increment (state) {
				state.counter++
			}
		},
		position: ["1 2 3"],
		isActive: true,
		hello: "hello"

	})
}

export default createStore