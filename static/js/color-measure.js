AFRAME.registerComponent('data-sender', {
  schema: {type: 'string'},

  init: function () {
  	this.el.addEventListener('mouseenter', function(evt) {
    	document.querySelector("a-text#L-value-field").setAttribute('value', '' + evt.detail.intersection.point.y.toFixed(2))
    	document.querySelector("a-text#a-value-field").setAttribute('value', '' + evt.detail.intersection.point.x.toFixed(2))
    	document.querySelector("a-text#b-value-field").setAttribute('value', '' + evt.detail.intersection.point.z.toFixed(2))
    	document.querySelector("a-entity#color-value-field").setAttribute('material', 'color: ' + evt.detail.intersection.object.el.components.material.attrValue.color)
  	})
  }
})